
CXX_FLAGS=$(shell geant4-config --cflags)
LD_FLAGS=$(shell geant4-config --libs)

all: mocktron

mocktron: main.o Mocktron.o Mockcess.o MockPhysics.o
	g++ $^ $(LD_FLAGS) -o $@

%.o: %.cc
	g++ $^ $(CXX_FLAGS) -c -o $@

clean:
	rm -f footrino
	rm -f *.o

.PHONY: clean
