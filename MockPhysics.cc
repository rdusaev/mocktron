#include "MockPhysics.hh"
#include "Mockcess.hh"

#include <G4StepLimiter.hh>
#include <G4Electron.hh>
#include <G4BuilderType.hh>

MockPhysics::MockPhysics() 
        : G4VPhysicsConstructor("MockPhysics") {
    SetPhysicsType(bUnknown);
    //fMessenger = new MockPhysicsMessenger();
}

void
MockPhysics::ConstructParticle() {
    // This call to particle definition must be first or at least go before
    // MockPhysics::CosntructProcess()
    Mocktron::Definition();
}

void
MockPhysics::ConstructProcess() {

    G4PhysicsListHelper * phLHelper = G4PhysicsListHelper::GetPhysicsListHelper();

    phLHelper->DumpOrdingParameterTable();

    // if one need to (re-)associate certain process with th particle, note
    // the following snippet
    //G4ProcessManager * pMgr = Mocktron::Definition()->GetProcessManager();
    //pmanager->RemoveProcess(idxt);
    //pmanager->AddProcess(new G4MonopoleTransportation(fMpl),-1, 0, 0);

    // ... here one can set up the model parameters from external config
    //     sources, internal attributes previously set by messengers, etc

    // ... here the processes asociated with new physics should be registered
    //     as follows
    phLHelper->RegisterProcess( new Mockcess
                              , G4Electron::ElectronDefinition() );
    phLHelper->RegisterProcess( new G4StepLimiter()
                              , Mocktron::Definition() );
}

