#pragma once

#include <G4VPhysicsConstructor.hh>

class MockPhysics : public G4VPhysicsConstructor {
public:
    MockPhysics();
    // Should call initial constructor of particle singletons
    virtual void ConstructParticle() override;
    virtual void ConstructProcess() override;
private:
    MockPhysics(const MockPhysics &) = delete;
    MockPhysics & operator=(const MockPhysics &) = delete;
};

