#include "Mockcess.hh"

#include <G4ProcessType.hh>
#include <G4EmProcessSubType.hh>

Mockcess::Mockcess()
            : G4VDiscreteProcess( "MockProduction"
                                , fUserDefined  // fElectromagnetic
                                )
            , theMocktronPtr( Mocktron::Definition() ) {
    SetProcessSubType( 1 //fBremsstrahlung  // TODO: verify this
                     );
}

G4bool
Mockcess::IsApplicable(const G4ParticleDefinition & pDef) {
    return "e-" == pDef.GetParticleName();
    //return fModelPtr->IsApplicableToParticle(pDef);
}

G4double
Mockcess::GetMeanFreePath( const G4Track & /*aTrack*/
                         , G4double /*previousStepSize*/
                         , G4ForceCondition * /*condition*/ ) {
    // one would like to use the `aTrack` object to get the incident particle
    // energy here to define real mean free path
    return 1e4;
}

G4VParticleChange *
Mockcess::PostStepDoIt( const G4Track & aTrack
			          , const G4Step & /*aStep*/ ) {
    const G4double incidentE = aTrack.GetKineticEnergy()
                 , mocktronMass = theMocktronPtr->GetPDGMass()
                 ;

    //G4double uRandVal = G4RandFlat::shoot( norm );
    G4double mocktronTheta = G4RandFlat::shoot( 30*CLHEP::deg )
           , mocktronE = G4RandFlat::shoot( incidentE/2 )
           , mocktronPhi = G4RandFlat::shoot( 180*CLHEP::deg )
           ;
    // Compute other kinematic parameters of the system w.r.t. generated
    // particle
    const G4double recoilE = incidentE - mocktronE
                 , recoilTheta = sqrt( mocktronMass / incidentE )
                               * (1 + mocktronMass / (2*incidentE)
                                 /* + ... todo: series? */ )
                 ;
    // Initialize A' direction vector:
    G4ThreeVector mocktronDirection(0., 0., .1); {
        mocktronDirection.setMag(1.);
        mocktronDirection.setTheta( mocktronTheta );
        mocktronDirection.setPhi( mocktronPhi );
    }
    // Initialize new projectile particle direction vector:
    G4ThreeVector projDirection(0., 0., 1); {
        projDirection.setMag(1.);
        projDirection.setTheta( recoilTheta );
        projDirection.setPhi( - mocktronPhi );
    }
    G4DynamicParticle * movingMocktron =  new G4DynamicParticle( theMocktronPtr
                                                               , mocktronDirection
                                                               , mocktronE );
    aParticleChange.Initialize( aTrack );

    // Set A':
    aParticleChange.SetNumberOfSecondaries( 1 );
    aParticleChange.AddSecondary( movingMocktron );
    // Set projectile changes:
    aParticleChange.ProposeEnergy( recoilE );
    aParticleChange.ProposeMomentumDirection( projDirection );

    std::cout << "Mocktron gone." << std::endl;

    return &aParticleChange;
}

