#pragma once

#include <G4VDiscreteProcess.hh>
#include "Mocktron.hh"

class Mockcess : public G4VDiscreteProcess {
public:
    Mocktron * theMocktronPtr;
public:
    Mockcess();
    // Implements final state parameters when process won.
    virtual G4VParticleChange* PostStepDoIt( const G4Track & ,
			                                 const G4Step & ) override;
    // Returns event probability, \f$mm^2\f$.
    virtual G4double GetMeanFreePath( const G4Track & aTrack,
                                      G4double previousStepSize,
                                      G4ForceCondition * condition ) override;
    // Returns `true` for incident particles under consideration.
    virtual G4bool IsApplicable(const G4ParticleDefinition &) override;
};  // class AMixingProcess

