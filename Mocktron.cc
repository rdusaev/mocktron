#include "Mocktron.hh"

#include <G4ParticleTable.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhaseSpaceDecayChannel.hh>
#include <G4DalitzDecayChannel.hh>
#include <G4DecayTable.hh>

Mocktron * Mocktron::theInstance = nullptr;

Mocktron *
Mocktron::Definition() {
    if( theInstance ) {
        return theInstance;
    }
    const G4String name = "Mocktron";
    // search in particle table]
    G4ParticleTable * pTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition * anInstance = pTable->FindParticle(name);
    if( !anInstance ) {
        anInstance = new G4ParticleDefinition(
                /* Name ..................... */ name,
                /* Mass ..................... */ 0.2*GeV,
                /* Decay width .............. */ 0.,  // TODO: decay width
                /* Charge ................... */ 0.*eplus,
		        /* 2*spin ................... */ 2,
                /* parity ................... */ 0,
                /* C-conjugation ............ */ 0,
		        /* 2*Isospin ................ */ 0,
                /* 2*Isospin3 ............... */ 0,
                /* G-parity ................. */ 0,
	            /* type ..................... */ "boson",
                /* lepton number ............ */ 0,
                /* baryon number ............ */ 0,
                /* PDG encoding ............. */ 88,
		        /* stable ................... */ true,  // TODO: enable on decay
                /* lifetime.................. */ 0,
                /* decay table .............. */ NULL,  // TODO: set on decay
                /* shortlived ............... */ false,  // TODO: set for short-living
                /* subType .................. */ "geantino",
                /* anti particle encoding ... */ 90    // TODO: ???
            );
    }
    theInstance = reinterpret_cast<Mocktron*>(anInstance);
    return theInstance;
}

Mocktron *
Mocktron::FootrinoDefinition() {
    return Definition();
}

