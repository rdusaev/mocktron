#pragma once

#include <G4ParticleDefinition.hh>

class Mocktron : public G4ParticleDefinition {
private:
    static Mocktron * theInstance;
    Mocktron();
    ~Mocktron();
public:
    static Mocktron * Definition();
    static Mocktron * FootrinoDefinition();
};


