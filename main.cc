#include <G4RunManager.hh>
#include <G4VisExecutive.hh>
#include <G4UIExecutive.hh>
#include <G4UImanager.hh>
#include <G4VUserPrimaryGeneratorAction.hh>
#include <G4ParticleGun.hh>
#include <G4SystemOfUnits.hh>
#include <G4VUserDetectorConstruction.hh>
#include <G4NistManager.hh>
#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4PVPlacement.hh>

#include <QBBC.hh>
#include <QGSP_BERT.hh>

#include <G4PhysListFactory.hh>

#include "MockPhysics.hh"

/// Minimal dummy primary generator action.
class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
    PrimaryGeneratorAction() {
        G4int n_particle = 1;
        _particleGunPtr = new G4ParticleGun(n_particle);

        G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
        G4String particleName;
        _particleGunPtr->SetParticleDefinition(
                   particleTable->FindParticle(particleName="e-"));
        _particleGunPtr->SetParticleEnergy(1.0*GeV);
        _particleGunPtr->SetParticlePosition(G4ThreeVector(0, 0, -4.*cm));
    }

    ~PrimaryGeneratorAction() {
        delete _particleGunPtr;
    }

    virtual void GeneratePrimaries(G4Event* anEvent) override {
        G4ThreeVector v(0.0,0.0,1.0);
        _particleGunPtr->SetParticleMomentumDirection(v);
        _particleGunPtr->GeneratePrimaryVertex(anEvent);
    }
private:
    G4ParticleGun * _particleGunPtr;
};

// A Pb cylinder within rectangular world filled with air
class DetectorConstruction : public G4VUserDetectorConstruction {
public:
    virtual G4VPhysicalVolume * Construct() override {
        G4NistManager * nist = G4NistManager::Instance();

        G4Material * matAir = nist->FindOrBuildMaterial("G4_AIR")
                 , * matLed = nist->FindOrBuildMaterial("G4_Pb");

        G4CSGSolid * solidWorld = new G4Box("World", 10*cm, 10*cm, 100*cm  );
        G4LogicalVolume * logWorld = new G4LogicalVolume( solidWorld, matAir, "World");
        G4VPhysicalVolume * physWorld = new G4PVPlacement( 0                // rot
                                                         , G4ThreeVector()  // placement
                                                         , logWorld         // logic volume
                                                         , "World"          // phys vol name
                                                         , nullptr          // mother vol ptr
                                                         , false            // no boolean op
                                                         , 0                // copy num
                                                         , false            // do overlaps check
                                                         );

        G4CSGSolid * solidTarget = new G4Tubs("Target", 0, 4*cm, 1*cm, 0, 2*M_PI  );
        G4LogicalVolume * logTarget = new G4LogicalVolume( solidTarget, matLed, "Target");
        /*G4VPhysicalVolume * physTarget =*/ new G4PVPlacement( 0                // rot
                                                          , G4ThreeVector()  // placement
                                                          , logTarget        // logic volume
                                                          , "Target"         // phys vol name
                                                          , logWorld         // mother vol ptr
                                                          , false            // no boolean op
                                                          , 0                // copy num
                                                          , false            // do overlaps check
                                                          );

        return physWorld;
    }
};



int
main(int argc, char * argv[]) {
    auto runMgr = new G4RunManager();
    runMgr->SetUserInitialization( new DetectorConstruction );  // init geom

    // ___ Here the "extension" part starts ___
    G4PhysListFactory factory;
    G4VModularPhysicsList * phys = factory.GetReferencePhysList("QGSP_BERT");
    // ^^^ most of the standard physics lists are available by this interface
    MockPhysics * mockPhysics = new MockPhysics();
    phys->RegisterPhysics(mockPhysics);
    // ^^^ Here the "extension" part ends ^^^

    runMgr->SetUserInitialization(phys);  // init phys

    runMgr->SetUserAction( new PrimaryGeneratorAction ); // init PGA
    runMgr->Initialize();

    G4VisExecutive * ve = new G4VisExecutive();
    ve->Initialize( /* former syntax required argc, argv */ );
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
    G4UImanager::GetUIpointer()->ApplyCommand("/control/execute vis.mac");
    ui->SessionStart();
    delete ui;
    delete ve;
    delete runMgr;
    return EXIT_SUCCESS;
}


